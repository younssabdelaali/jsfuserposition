/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entities.Position;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author user
 */
@Stateless
public class PositionFacade extends AbstractFacade<Position> {
    @PersistenceContext(unitName = "WebApplicationPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PositionFacade() {
        super(Position.class);
    }
    
      public List<Position> getbydates(Date d1 , Date d2){
        List <Position> positions = new ArrayList<>();
           TypedQuery<Position> q  =  getEntityManager().createQuery("from Position m where m.date between :d1 and :d2",Position.class).setParameter("d1", d1).setParameter("d2", d2);
        
        return q.getResultList();
        
    }
      
       public List<Position> getPositionUser(int nmb){
        List <Position> positions = new ArrayList<>();
           TypedQuery<Position> q  =  getEntityManager().createQuery("from Position m where m.userId.id =:d1",Position.class).setParameter("d1", nmb);
        
        return q.getResultList();
        
    }
      
     
}
